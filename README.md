# Datasets

We will have datasets here, only csv files. For now only Robert and Patricia should approve a merge request into `main` to ensure the rules are adhered to (we haven't written down the rules yet).

All files will be numbered `0000_*.csv`.